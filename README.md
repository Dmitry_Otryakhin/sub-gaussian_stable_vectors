# Sub-Gaussian_Stable_Vectors

This repository contains supplementary materials for article Estimation of Sub-Gaussian Random Vectors Using the Method of Moments.

These consist of a script for reproduction of the numerical study and the newest development version of sgstabMM package required by the script. 

The former is located in subfolder /Executive_scripts/num_study_short.Rmd and is rendered in num_study_short.pdf file.

The package provides functions for simulation of Sub-Gaussian Random vectors and parameter estimation developed in the paper.
