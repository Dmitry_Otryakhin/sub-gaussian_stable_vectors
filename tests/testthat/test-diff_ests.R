context("Composite estimators return adequate results")
library(sgstabMM)

################################################################################

N<-1e5
alpha<-1.8

sigma_vect <- c(1.2, 0.4, 0.1, 0.4, 1.7, 0.2, 0.1, 0.2, 0.7) *1e-1
#sigma_vect <- c(1, 0, 0, 0, 1, 0, 0, 0, 1) *1e-1

l_sig <- length(sigma_vect)
sigma<-matrix(data=sigma_vect, nrow = sqrt(l_sig), ncol = sqrt(l_sig), byrow = FALSE)
delta=rep(0, length=sqrt(l_sig))

dd <- mvarSG(n=N, alpha=alpha, sigma=sigma, delta=0)
s_1 <- seq(-1e0, 1e0, by=5e-3)

est<-Multt_sig_diag_est(data=dd, alpha=alpha, s_1=s_1)
rr<-abs(diag(sigma - est)) < 0.2*diag(sigma)

test_that("Multt diag estimator should return a number close to diag(sigma)", {
  expect_true(rr[1]&rr[2]&rr[3])
})

################################################################################

N<-1e6
alpha<-1.2
dd <- mvarSG(n=N, alpha=alpha, sigma=sigma, delta=0)

est<-Multt_sig_diag_est(data=dd, alpha=alpha, s_1=s_1)
rr<-abs(diag(sigma - est)) < 2*diag(sigma)

test_that("Multt diag estimator should return a number close to diag(sigma)", {
  expect_true(rr[1]&rr[2]&rr[3])
})

################################################################################




